package ru.t1.stepanishchev.tm.exception.user;

public final class AccessDeniedException extends AbstractUserException {

    public AccessDeniedException() {
        super("Error! You're not logged in. Please try again.");
    }

}