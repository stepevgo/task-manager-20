package ru.t1.stepanishchev.tm.repository;

import ru.t1.stepanishchev.tm.api.repository.IProjectRepository;
import ru.t1.stepanishchev.tm.model.Project;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    public Project create(final String userId, final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @Override
    public Project create(final String userId, final String name) {
        final Project project = new Project();
        project.setName(name);
        return add(project);
    }

}