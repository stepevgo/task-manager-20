package ru.t1.stepanishchev.tm.repository;

import ru.t1.stepanishchev.tm.api.repository.IUserOwnedRepository;
import ru.t1.stepanishchev.tm.enumerated.Sort;
import ru.t1.stepanishchev.tm.exception.AbstractException;
import ru.t1.stepanishchev.tm.exception.entity.ModelNotFoundException;
import ru.t1.stepanishchev.tm.model.AbstractUserOwnedModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public M add(final String userId, final M record) {
        record.setUserId(userId);
        return add(record);
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null) return Collections.emptyList();
        final List<M> result = new ArrayList<>();
        for (final M m : models) {
            if (userId.equals(m.getUserId())) {
                result.add(m);
            }
        }
        return result;
    }

    @Override
    public List<M> findAll(final String userId, final Comparator comparator) {
        final List<M> models = findAll(userId);
        models.sort(comparator);
        return models;
    }

    @Override
    public List<M> findAll(final String userId, final Sort sort) {
        return super.findAll(sort);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public M findOneById(final String userId, final String id) throws AbstractException {
        final List<M> userRecords = findAll(userId);
        for (final M model : userRecords) {
            if (model.getId().equals(id)) {
                return model;
            }
        }
        throw new ModelNotFoundException();
    }

    @Override
    public boolean existsById(final String userId, final String id) throws AbstractException {
        findOneById(userId, id);
        return true;
    }

    @Override
    public M remove(final String userId, final M model) {
        if (userId.equals(model.getUserId())) {
            models.remove(model);
        }
        return model;
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) throws ModelNotFoundException {
        final M model = findOneByIndex(userId, index);
        if (model == null) throw new ModelNotFoundException();
        return remove(userId, model);
    }

    @Override
    public M removeById(final String userId, final String id) throws AbstractException {
        final M model = findOneById(userId, id);
        return remove(userId, model);
    }

    @Override
    public void clear(final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

}