package ru.t1.stepanishchev.tm.api.service;

import ru.t1.stepanishchev.tm.api.repository.IUserOwnedRepository;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.model.Project;

public interface IProjectService extends IUserOwnedRepository<Project> {

    Project changeProjectStatusById(String userId, String id, Status status);

    Project changeProjectStatusByIndex(String userId, Integer index, Status status);

    Project create(String userId, String name, String description);

    Project create(String userId, String name);

    Project updateById(String userId, String id, String name, String description);

    Project updateByIndex(String userId, Integer index, String name, String description);

}