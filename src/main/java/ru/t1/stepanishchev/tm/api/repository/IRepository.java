package ru.t1.stepanishchev.tm.api.repository;

import ru.t1.stepanishchev.tm.enumerated.Sort;
import ru.t1.stepanishchev.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    List<M> findAll(Sort sort);

    void removeAll(Collection<M> collection);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    M remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

    void clear();

    boolean existsById(String id);

}