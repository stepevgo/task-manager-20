package ru.t1.stepanishchev.tm.api.service;

import ru.t1.stepanishchev.tm.api.repository.IUserOwnedRepository;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedRepository<Task> {

    Task changeTaskStatusById(String userId, String id, Status status);

    Task changeTaskStatusByIndex(String userId, Integer index, Status status);

    Task create(String userId, String name, String description);

    Task create(String userId, String name);

    List<Task> findAllByProjectId(String userId, String projectId);

    Task updateById(String userId, String id, String name, String description);

    Task updateByIndex(String userId, Integer index, String name, String description);

}