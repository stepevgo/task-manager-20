package ru.t1.stepanishchev.tm.api.repository;

import ru.t1.stepanishchev.tm.enumerated.Sort;
import ru.t1.stepanishchev.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    M add(String userId, M model);

    List<M> findAll(final String userId);

    List<M> findAll(final String userId, final Comparator comparator);

    List<M> findAll(final String userId, final Sort sort);

    M findOneByIndex(final String userId, final Integer index);

    M findOneById(final String userId, final String id);

    boolean existsById(final String userId, final String id);

    M remove(final String userId, final M model);

    M removeByIndex(final String userId, final Integer index);

    M removeById(final String userId, final String id);

    void clear(final String userId);

}