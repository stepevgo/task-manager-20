package ru.t1.stepanishchev.tm.command.project;

import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    private final String NAME = "project-completed-by-id";

    private final String DESCRIPTION = "Complete project by id.";

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("[ENTER ID: ]");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().changeProjectStatusById(userId, id, Status.COMPLETED);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}