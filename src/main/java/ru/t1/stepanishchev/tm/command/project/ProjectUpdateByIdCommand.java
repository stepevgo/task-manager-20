package ru.t1.stepanishchev.tm.command.project;

import ru.t1.stepanishchev.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    private final String NAME = "project-update-by-id";

    private final String DESCRIPTION = "Update project by id.";

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().updateById(userId, id, name, description);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}