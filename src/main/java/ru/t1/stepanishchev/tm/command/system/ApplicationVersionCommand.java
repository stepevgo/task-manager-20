package ru.t1.stepanishchev.tm.command.system;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    private final String NAME = "version";

    private final String ARGUMENT = "-v";

    private final String DESCRIPTION = "Show app version.";

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("version: 1.20.0");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}