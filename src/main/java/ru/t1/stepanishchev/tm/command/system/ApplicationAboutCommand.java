package ru.t1.stepanishchev.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    private final String NAME = "about";

    private final String ARGUMENT = "-a";

    private final String DESCRIPTION = "Show info about developer.";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Evgeniy Stepanishchev");
        System.out.println("e-mail: estepanischev@t1-consulting.ru");
        System.out.println("e-mail: stepevgo.vrn@gmail.com");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}