package ru.t1.stepanishchev.tm.command.task;

import ru.t1.stepanishchev.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    private final String NAME = "task-bind-to-project";

    private final String DESCRIPTION = "Bind task to project.";

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}