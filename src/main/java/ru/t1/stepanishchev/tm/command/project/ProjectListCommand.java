package ru.t1.stepanishchev.tm.command.project;

import ru.t1.stepanishchev.tm.enumerated.Sort;
import ru.t1.stepanishchev.tm.model.Project;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    private final String NAME = "project-list";

    private final String DESCRIPTION = "Show list projects.";

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final String userId = getUserId();
        final List<Project> projects = getProjectService().findAll(userId, sort);
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName() + " : " + project.getId());
            index++;
        }
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}