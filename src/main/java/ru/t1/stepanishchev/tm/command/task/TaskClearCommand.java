package ru.t1.stepanishchev.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand {

    private final String NAME = "task-clear";

    private final String DESCRIPTION = "Remove all tasks.";

    @Override
    public void execute() {
        System.out.println("[TASKS CLEAR]");
        final String userId = getUserId();
        getTaskService().clear(userId);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}